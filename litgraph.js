import {
  LitElement,
  html,
} from "https://cdn.jsdelivr.net/gh/lit/dist@3/core/lit-core.min.js";
import "https://d3js.org/d3.v6.js";

// Get API key from https://octopus.energy/dashboard/new/accounts/personal-details/api-access
const apiKey = "";
const drawAreaSelector = "#drawArea";
const dateParser = d3.timeParse("%Y-%m-%dT%H:%M:%S%Z");

const dateFormatter = (date) => {
  const hourMin = d3.utcFormat("%H:%M")(date);
  if (hourMin !== "00:00") {
    return hourMin;
  }
  return d3.utcFormat("%m-%d %H:%M")(date);
};

export class EnergyGraph extends LitElement {
  static properties = {
    margin: { type: Object },
    width: { type: Number },
    height: { type: Number },
    xAxis: { type: Object },
    yAxis: { type: Object },
    svg: { type: Object },
  };

  constructor() {
    super();
    this.margin = { top: 30, right: 30, bottom: 150, left: 30 };
    this.width = 800 - this.margin.left - this.margin.right;
    this.height = 480 - this.margin.top - this.margin.bottom;
    this.xAxis = d3.scaleBand().range([0, this.width]).padding(0.1);
    this.yAxis = d3.scaleLinear().range([this.height, 0]);
  }

  render() {
    return html`
      <input type="button" value="refresh" @click=${this.loadChart} />
      <div id="${drawAreaSelector.substring(1)}"></div>
    `;
  }

  connectedCallback() {
    super.connectedCallback();
    this.loadChart();
  }

  loadChart() {
    this.downloadData()
      .then((_) => console.log(this.data))
      .then((_) => this.drawChart());
  }

  async downloadData() {
    const today = new Date();
    const yesterday = new Date();
    yesterday.setDate(today.getDate() - 1);

    const url =
      "https://api.octopus.energy/v1/electricity-meter-points/2200015326360/meters/22L3624001/consumption/";
    const headers = new Headers();
    headers.set("Authorization", "Basic " + btoa(`${apiKey}:`));

    //Read the data
    await fetch(`${url}?period_from=${yesterday.toISOString().split("T")[0]}`, {
      method: "GET",
      headers: headers,
    })
      .then((response) => response.json())
      .then((data) => data.results)
      .then((results) =>
        results.map((r) => ({
          date: dateFormatter(dateParser(r.interval_end)),
          value: r.consumption,
        })),
      )
      .then((r) => (this.data = r));
  }

  drawChart() {
    const element = this.shadowRoot.querySelector(drawAreaSelector);
    // append the svg object to the body of the page
    this.svg = d3
      .select(element)
      .append("svg")
      .attr("width", this.width + this.margin.left + this.margin.right)
      .attr("height", this.height + this.margin.top + this.margin.bottom)
      .append("g")
      .attr(
        "transform",
        "translate(" + this.margin.left + "," + this.margin.top + ")",
      );

    // X axis
    const x = d3
      .scaleBand()
      .range([0, this.width])
      .domain(this.data.map((d) => d.date))
      .padding(0.2);
    this.svg
      .append("g")
      .attr("transform", `translate(0, ${this.height})`)
      .call(d3.axisBottom(x))
      .selectAll("text")
      .attr("transform", "translate(-10,0)rotate(-45)")
      .style("text-anchor", "end");

    // Add Y axis
    const y = d3
      .scaleLinear()
      .domain(d3.extent(this.data, (d) => d.value))
      .range([this.height, 0]);
    this.svg.append("g").call(d3.axisLeft(y));

    // Bars
    this.svg
      .selectAll("mybar")
      .data(this.data)
      .join("rect")
      .attr("x", (d) => x(d.date))
      // .attr("x", (d) => x(d.date))
      .attr("y", (d) => y(d.value))
      .attr("width", x.bandwidth())
      .attr("height", (d) => this.height - y(d.value))
      .attr("fill", "#f050f8");
  }
}

customElements.define("energy-graph", EnergyGraph);
